
     <div id="page-content-wrapper">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark border-bottom p-0">

        <div class="sidebar-logo">
            <a href="<@ofbizUrl>home</@ofbizUrl>">
                <img src="../static/logo/AP-logo-1.png" height="55px" class="mxSidebarLogo ml-2"></a>
        </div>

        <div class="sidebar-heading bg-dark">
          <a href="<@ofbizUrl>home</@ofbizUrl>">
            <img src="../static/logo/AP-logo-text.png" height="55px" width="100px" class="mxSidebarLogoText ml-1"></a>
        </div>
    
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <#if orgParty??>
            <div class="pl-3">
                <b>${Static["org.apache.ofbiz.party.party.PartyHelper"].getPartyName(orgParty)}</b>
                <span class="small text-muted">(${tenantId!})</span>
            </div>
        </#if>

        <div class="bg-dark navbar-collapse pull-right collapse" id="navbarSupportedContent" style="z-index: 999;">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <#--  <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>  -->
            <#--  <li class="nav-item">
                <a class="nav-link" href="#">Contact</a>
            </li>  -->
            
            <div class="nav-item toastMsg pull-right alert alert-secondary m-1" role="alert" style="display:none;"></div>
            
            <div class="nav-item text p-2" style="cursor: pointer;">
                <span class="nav-link goBack px-2" onclick='window.history.back();'><u>Close Editor</u></span>
            </div>
            
            <#if userLogin??>
            <li class="nav-item dropdown p-2">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons icon-midnightblue">account_circle</i>
                    ${Static["org.apache.ofbiz.party.party.PartyHelper"].getPartyName(loggedInParty)}
                </a>
                <div class="dropdown-menu dropdown-menu-right mr-1" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item disabled">
                        <i class="material-icons icon-darkred">beenhere</i> <i class="userRoleName">${userRoleName!}</i>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<@ofbizUrl>myaccount</@ofbizUrl>"><i class="material-icons">perm_identity</i> My Account</a>
                    <a class="dropdown-item" href="<@ofbizUrl>changePassword</@ofbizUrl>"><i class="material-icons">security</i> Change Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<@ofbizUrl>logout</@ofbizUrl>"><i class="material-icons icon-darkgray">exit_to_app</i> Logout</a>
                </div>
            </li>
            <#else>
                <li class="nav-item">
                    <a class="nav-link" href="<@ofbizUrl>login</@ofbizUrl>">Sign in</a>
                </li>
            </#if>
            </ul>
        </div>
    </nav>


<#if requestAttributes.errorMessageList?has_content><#assign errorMessageList=requestAttributes.errorMessageList></#if>
<#if requestAttributes.eventMessageList?has_content><#assign eventMessageList=requestAttributes.eventMessageList></#if>
<#if requestAttributes.serviceValidationException??><#assign serviceValidationException = requestAttributes.serviceValidationException></#if>
<#if requestAttributes.uiLabelMap?has_content><#assign uiLabelMap = requestAttributes.uiLabelMap></#if>

<#if !errorMessage?has_content>
    <#assign errorMessage = requestAttributes._ERROR_MESSAGE_!>
</#if>
<#if !errorMessageList?has_content>
    <#assign errorMessageList = requestAttributes._ERROR_MESSAGE_LIST_!>
</#if>
<#if !eventMessage?has_content>
    <#assign eventMessage = requestAttributes._EVENT_MESSAGE_!>
</#if>
<#if !eventMessageList?has_content>
    <#assign eventMessageList = requestAttributes._EVENT_MESSAGE_LIST_!>
</#if>
   