<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="apple-touch-icon" sizes="180x180" href="../static/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../static/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../static/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="../static/images/favicon/site.webmanifest">

  <#if title??>
    <title>${title!} - AutoPatt Console</title>
    <#else>
      <title>AutoPatt Console</title>
  </#if>

  <!-- Bootstrap core CSS -->
  <link href="../static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../static/css/portal-main.css" rel="stylesheet">

  <link href="../static/css/md_icon.css" rel="stylesheet">

  <link rel="stylesheet" href="../static/css/font-awesome.min.css">

  <#-- Tag input -->
    <link rel="stylesheet" href="../static/css/tagsinput.css" />

</head>

<script>
  (function () {
    window.onpageshow = function (event) {
      if (event.persisted) {
        window.location.href = window.location.href;
      }
    };
  })();
</script>

<body>
  <div class="text-center" id="page_loading" style="padding:50px;">
    <div class="spinner-border" role="status"></div>
  </div>

  <div class="d-flex" id="wrapper" style="display:none !important;">