<style>
  .modal {
    overflow-y: auto;
  }

  #viewDeploymentSummaryModal>.modal-lg {
    width: 90% !important;
    height: 90% !important;
    margin: 2% 5% !important;
    padding: 0 !important;
    max-width: none !important;
  }

  #viewDeploymentSummaryModal>.modal-lg>.modal-content {
    height: auto !important;
    min-height: 80% !important;
  }

  #viewDeploymentSummaryModal>.modal-lg>.modal-header {
    border-bottom: 1px solid #9ea2a2 !important;
  }

  #viewDeploymentSummaryModal>.modal-lg>.modal-footer {
    border-top: 1px solid #9ea2a2 !important;
  }
</style>

<div class="container-fluid">
  <div class="row py-3">
    <div class="navigationControl ml-3 h6 m-0">
      <a href='javascript:window.history.back();'><span class="previousPage text-decoration-underline">
          < Back</span> </a> | <a href='javascript:window.history.forward();'><span class="nextPage">Next ></span></a>
    </div><br>
    <div class="col-12 pb-2 h3 text-center title">Solution Design</div>
    ​<div class="col-12 p-3">
      <div class="form-group probStatementForm">
        <a href="#" class="text-dark probStatementLink"><span class="text-justify probStatement h5"></span> <i
            class="fa fa-link linkIcon icon-blue" aria-hidden="true" style="display:none;"></i></a>
        <p class="text-justify probStatementDescription"></p>
        <hr class="my-4">
      </div>
      <div class="form-group basePatternForm">
        <div class="row">
          <div class="col-10 pr-0">
            <a href="#" class="text-dark basePatternLink"><span class="text-justify basePattern h5"></span> <i
                class="fa fa-link linkIconPT icon-blue" aria-hidden="true" style="display:none;"></i></a>
          </div>
          <div class="col-2 text-center">
            <span class="typeDataBP"></span>
            <div class="mt-2">
              <a class="btn btn-outline-dark p-1 viewBpImage" data-toggle="tooltip" data-placement="left"
                title="View Pattern" href="javascript:void(0);">
                <i class="fa fa-picture-o fa-2x" data-target="#modalIMG" data-toggle="modal" aria-hidden="true"></i></a>
            </div>
          </div>
          <div class="col-12">
            <p class="p-1 text-justify basePatternDescription"></p>
          </div>
          <hr class="my-4">
        </div>
      </div>
      <div class="form-group solutionDesignForm">
        <div class="row">
          <div class="col-10 pr-0">
            <label class="solutionDesign text-justify h5"></label>
          </div>
          <div class="col-2 text-center">
            <span class="typeDataSD"></span>
            <div class="mt-2">
              <span data-toggle="modal" data-target="#viewDeploymentSummaryModal">
                <a class="btn btn-outline-primary m-1 viewDeploymentSummaryBtn" href="javascript:void(0);"
                  aria-label="viewDeploymentSummary" data-toggle="tooltip" data-placement="left"
                  title="Deployment Logs">
                  <i class="fa fa-list-alt fa-lg" aria-hidden="true"></i></a>
              </span>

              <span data-toggle="modal" data-target="#editFormModal">
                <a class="btn btn-outline-primary m-1 editSD" href="javascript:void(0);" aria-label="Edit"
                  data-toggle="tooltip" data-placement="top" title="Edit data">
                  <i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
              </span>

              <a class="btn btn-outline-danger m-1 deleteSD" data-toggle="tooltip" data-placement="top"
                title="Delete Solution Design" href="javascript:void(0);" aria-label="Delete">
                <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a>

            </div>
          </div>
        </div>
        <div class="p-3 row">
          <p class="col-12 p-1 solutionDesignDescription text-justify"></p>
          <div class="col-6 border rounded">
            <label for="solutionDesignForces h5"><b>Forces</b></label>
            <hr class="m-0">
            <p class="solutionDesignForces"></p>
          </div>
          <div class="col-6 border rounded">
            <label for="solutionDesignConsequences"><b>Consequences</b></label>
            <hr class="m-0">
            <p class="solutionDesignConsequences"></p>
          </div>
        </div>
      </div>
      <div class="col-12 px-3 allActionBtns">
        <div class="toastMsg text-center m-0"></div>
        <#-- toastMsgForSolutionDesignCheck -->
          <div class="text-center p-2" id="allButtonsDiv">
            <span class="" tabindex="0" data-toggle="tooltip" data-placement="left" title="Approve Design">
              <button class="btn btn-primary m-1 p-1 approve" id='approveBtn' style="width: 100px;">Approve</button>
            </span>
            <button class="btn btn-primary m-1 p-1 requestApprove" id='requestApproveBtn'
              style="width: 100px;display:none;" tabindex="0" data-toggle="tooltip" data-placement="top"
              title="Request to Approve Design">Request</button>
            <button class="btn btn-primary m-1 p-1 edit" id='editBtn' style="width: 100px;" tabindex="0"
              data-toggle="tooltip" data-placement="top" title="Edit Design">
              <i class="fa fa-pencil" aria-hidden="true"></i></button>
            <span class="deployCheck">
              <button class="btn btn-primary m-1 p-1 deploy" id='deployBtn' style="width: 100px;" type="button"
                tabindex="0" data-toggle="tooltip" data-placement="right" title="Deploy Design">Deploy</button>
            </span>
          </div>
          <#--<img src="" srcset="" class="img-fluid img-thumbnail w-100 h-100" alt="..." id="solutionDesignImg">-->
                <#--  <span class="pr-2 pull-right maximizeImage" data-toggle="modal" data-target="#maximizeSDModal">
                <i class="fa fa-window-maximize" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Maximise"></i>
                </span>  -->  
          <span class="pr-2 pt-1 pull-right viewAttributes" data-toggle="modal" data-target="#viewAttributesModal">
            <i class="fa fa-list-alt fa-lg" aria-hidden="true" data-toggle="tooltip" data-placement="top"
              title="View Attributes"></i>
          </span>
          <div class='text-center svgDiv img-fluid img-thumbnail w-100 p-2' style="height:auto;"></div>
      </div>
    </div>
  </div>

  <#-- Show Pattern Image -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalIMG" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body mb-0 p-3">
            <#-- <img src="" alt="" style="width:100%"> -->
              <div class='BPsvgDiv img-fluid img-thumbnail p-1' style="overflow:auto;"></div>
          </div>
          <div class="modal-footer">
            Status :&nbsp;<span class="BPStatus"></span>
            <button class="btn btn-outline-danger btn-rounded btn-md ml-4 text-center" data-dismiss="modal"
              type="button">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editFormModal" tabindex="-1" role="dialog" aria-labelledby="editFormModalTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="editFormModalTitle">Edit User Defined Design</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modalBody">
            <form id="solutionDesignPatternForm">
              <div class="form-group">
                <label for="solutionDesignName">Solution Design Name</label>
                <div class="input-container">
                  <textarea type="text" name="Solution Design Name" class="form-control" required
                    id="solutionDesignName"></textarea>
                </div>
              </div>
              <input type="hidden" class="psid" name="psid" id="psid" value="">
              <div class="form-group">
                <label for="solutionDesignDesc">Description</label>
                <div class="input-container">
                  <textarea class="form-control" name="Description" required
                    id="solutionDesignDesc" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label>Forces</label>
                <div class="input-container">
                  <input class="form-control" name="Forces" required id="solutionForces" rows="3" />
                </div>
              </div>
              <div class="form-group">
                <label>Consequences</label>
                <div class="input-container">
                  <input class="form-control" name="Consequences" required id="solutionConsequences"
                    rows="3" />
                </div>
              </div>
              <div class="formToastMsg my-1"></div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"
              id="closeBtnForEditModal">Close</button>
            <button type="button" class="btn btn-primary saveChangesBtn" id="saveChangesBtn">Save changes</button>
          </div>
        </div>
      </div>
    </div>

    <!-- View Deployment Summary Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewDeploymentSummaryModal" tabindex="-1" role="dialog"
      aria-labelledby="viewDeploymentSummaryModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="viewDeploymentSummaryModalTitle">Deployment Log Summary</h5>
            <span class="text-secondary ml-3 p-1 pull-right deploymentStatus"></span>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modalBody deploymentSummaryModalBody">
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link" id="nav-comments-tab" data-toggle="tab" href="#nav-comments" role="tab"
                  aria-controls="nav-comments" aria-selected="true">Comments</a>
                <a class="nav-item nav-link active" id="nav-compile-tab" data-toggle="tab" href="#nav-compile"
                  role="tab" aria-controls="nav-compile" aria-selected="false">Compile Logs</a>
                <a class="nav-item nav-link" id="nav-runtime-tab" data-toggle="tab" href="#nav-runtime" role="tab"
                  aria-controls="nav-runtime" aria-selected="false">Runtime Logs</a>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <#-- Comments Tab -->
                <div class="tab-pane fade show active" id="nav-comments" role="tabpanel"
                  aria-labelledby="nav-comments-tab">
                  <center class="h4 mt-2 commentsStatus text-secondary"></center>
                  <div class="commentsTabData p-2">

                  </div>
                  <div class="commentsTabDataInTableDiv">
                    <table class="table table-striped table-fit">
                      <thead>
                        <tr>
                          <th scope="col" style="width:18%">Time</th>
                          <th scope="col" style="width:25%">By</th>
                          <th scope="col" style="width:57%">Comments</th>
                        </tr>
                      </thead>
                      <tbody class="commentsTabTable">

                      </tbody>
                    </table>
                  </div>
                </div>
                <#-- Compile Logs Tab -->
                  <div class="tab-pane fade" id="nav-compile" role="tabpanel" aria-labelledby="nav-compile-tab">
                    <center class="h4 mt-2 compileStatus"></center>
                    <div class="compileTabData p-2">

                    </div>
                    <div class="compileTabDataInTableDiv">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Step Name</th>
                            <th scope="col">Component Data</th>
                            <th scope="col">Creation Details</th>
                            <th scope="col">Comments</th>
                          </tr>
                        </thead>
                        <tbody class="compileTabTable">

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <#-- Runtime Logs Tab -->
                    <div class="tab-pane fade" id="nav-runtime" role="tabpanel" aria-labelledby="nav-runtime-tab">
                      <center class="h4 mt-2 runtimeStatus"></center>
                      <div class="runtimeTabData p-2">

                      </div>
                      <div class="runtimeTabDataInTableDiv">
                        <table class="table table-striped table-condensed">
                          <thead>
                            <tr>
                              <th scope="col">Step Name</th>
                              <th scope="col">Component Data</th>
                              <th scope="col">Creation Details</th>
                              <th scope="col">Comments</th>
                            </tr>
                          </thead>
                          <tbody class="runtimeTabTable">

                          </tbody>
                        </table>
                      </div>
                    </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"
              id="closeBtnForDeploymentSummary">Close</button>
            <button type="button" class="btn btn-primary proceedBtn" id="proceedBtn">Proceed</button>
          </div>
        </div>
      </div>
    </div>

    <#-- Loading Spinner Modal -->
      <div class="modal fade" id="loadingSpinnerModal" tabindex="-1" role="dialog"
        aria-labelledby="loadingSpinnerModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i><span class="loadingSpinnerMsg"></span></p>
            </div>
          </div>
        </div>
      </div>


      <#-- Approval Reject Modal -->
        <div class="modal fade" id="approvalRejectModal" tabindex="-1" role="dialog"
          aria-labelledby="approvalRejectModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="approvalRejectModalLabel">Solution Design Approval</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body pb-0 mb-0">
                <center class="h5 mt-2">Please confirm to approve design or reject with comments.</center>
                <form>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Comments:</label>
                    <div class="input-container">
                      <textarea class="form-control" id="comments" name="Comments" required></textarea>
                    </div>
                    <div class="approveDivToastMsg pt-2"></div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger approvalRejectBtn">Reject</button>
                <button type="button" class="btn btn-primary approveBtn">Approve</button>
              </div>
            </div>
          </div>
        </div>

        <!-- View Attributes of Solution Design Modal -->
        <div class="modal fade bd-example-modal-lg" id="viewAttributesModal" tabindex="-1" role="dialog"
          aria-labelledby="viewAttributesModalTitle" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-body modalBody viewAttributesModalBody">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <center class="h4 mt-2 customAttributesStatus">Custom Attributes</center>
                <div class="border-bottom border-left border-right customAttributesTabTableDiv rounded">
                  <table class="table table-responsive-lg table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Component</th>
                        <th scope="col">Attributes</th>
                      </tr>
                    </thead>
                    <tbody class="customAttributesTabTable">

                    </tbody>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                Status :&nbsp;<span class="attributeStatus"></span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                  id="closeBtnForViewAttributesModal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <script type="module" src="../static/graphEditor/js/customJs/solutionDesign.js"></script>