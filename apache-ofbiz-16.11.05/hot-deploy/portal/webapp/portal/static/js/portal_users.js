import { App } from '../graphEditor/js/customJs/app.js';
import { showSuccessToast, showErrorToast } from './toast_utils.js';
import { initiateUsersMgmtModals } from './portal_modals.js';

$(function () {
    if ($('.userRoleName').text()) {
        $.ajax({
            method: "POST",
            url: "isLoggedIn",
            data: { "sdid": '' },
            success: function (data) {
                if (!(typeof data == "string" && data.startsWith("<!DOCTYPE html>"))) {
                    if (!data._IS_LOGGED_IN) {
                        window.location.href = 'login.ftl';
                    }
                }
            },
            error: function (err) { console.log(err); }
        });
    }

    $('#userEmail').blur(function () { checkIfEmailExists(); });

    $('#deleteUserConfirmModalRemoveBtn').on('click', function () { _removeUser(); });

    $('#removed_email_allow_reenable').on('click', function () { _reEnableUser(); });

    $('#activateUserConfirmModalActivateBtn').on('click', function () { activateUser(); });

    $('#suspendUserConfirmModalSuspendBtn').on('click', function () { suspendUser(); });

    $('#resetPasswordUserConfirmModalResetPswdBtn').on('click', function () { initResetUserPwd(); });

    checkSidebarAndNavbarIsCollapsed(); setSidebarAndNavbarHeight(true);

    // Sidebar : Active palette's icon change
    if ($('.list-group-item').hasClass('active')) {
        $('.list-group-item').filter(".active")[0].children[0].src = $('.list-group-item').filter(".active")[0].children[0].src.replace('.png', '-2.png');
        $('.list-group-item').filter(".active").addClass('h6 text-success');
    }
    if (location.pathname == "/portal/c/settings") {
        $('.settings-icon')[0].src = $('.settings-icon')[0].src.replace('.png', '-2.png');
    }
    $('#loginSidebarImage').css({ height: window.innerHeight });

    $(window).resize(function () {
        setSidebarAndNavbarHeight(false);
    });

    updatePassword();
});

function setSidebarAndNavbarHeight(isLoading) {
    let navbar = document.getElementsByClassName('navbar'), navbarHeight;

    if (!isLoading) {
        if (navbar.length > 0) { navbarHeight = document.getElementsByClassName('navbar')[0].scrollHeight; }
        $('.sidebarLogo').height(navbarHeight); $('.sidebarLogoText').height(navbarHeight);
        $('.sidebarLogo').css({ 'max-height': '72px' });
    } else {
        setTimeout(function () {
            if (navbar.length > 0) { navbarHeight = document.getElementsByClassName('navbar')[0].scrollHeight; }
            $('.sidebarLogo').height(navbarHeight); $('.sidebarLogoText').height(navbarHeight);
            $('.sidebarLogo').css({ 'max-height': '72px' });
        }, 400);
    }
}

function checkSidebarAndNavbarIsCollapsed() {
    setTimeout(function () {
        if ($("#wrapper").hasClass('collapse')) {
            $('.sidebar-heading').addClass('collapse');
            $("#menu-toggle-icon").attr('src', "../static/images/icon/menu.png");
        } else {
            $('.sidebar-heading').removeClass('collapse');
            $("#menu-toggle-icon").attr('src', "../static/images/icon/menu-2.png");
        }
    }, 200)

    $('#menu-toggle').on('click', function () {
        if (!$("#wrapper").hasClass('collapse')) {
            $("#menu-toggle-icon").attr('src', "../static/images/icon/menu-2.png");
            $('.sidebar-heading').removeClass('collapse');
        } else {
            $("#menu-toggle-icon").attr('src', "../static/images/icon/menu.png");
            $('.sidebar-heading').addClass('collapse');
        }
    });

    $('.navbar-toggler').on('click', function () {
        if ($("#navbarSupportedContent").hasClass('show')) {
            $("#navbar-toggle-icon").attr('src', "../static/images/icon/menu-2.png");
        } else {
            $("#navbar-toggle-icon").attr('src', "../static/images/icon/menu.png");
        }
    });
}

function _removeUser() {
    var userPartyId = $("#deleteUser_partyId").val()
    var postData = { userPartyId: App.xssValidate(userPartyId) };
    var formURL = $("#delete_user_form").attr("action");
    console.log(postData)
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                $('#deleteUserConfirmModal').modal('hide');
                showSuccessToast("User Deleted Successfully");
                _loadUsers();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
}

function _loadUsers() {
    $("#users_list_section").load(getAppUrl("users_list_section"), function () {
        initiateUsersMgmtModals();
    });
}

function activateUser() {
    var employeePartyId = $("#enableUser_partyId").val()
    var postData = { partyId: App.xssValidate(employeePartyId) };
    var formURL = $("#enable_user_form").attr("action");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (resp) {
                if (resp.Success === "Y") {
                    $('#activateUserConfirmModal').modal('hide');
                    showSuccessToast("User Activated Successfully");
                    setTimeout(function () {
                        _loadUsers();
                    }, 500);
                } else {
                    //showErrorToast("user cannot be activated");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
}
function suspendUser() {
    var employeePartyId = $("#suspendUser_partyId").val()
    var postData = { partyId: App.xssValidate(employeePartyId) };
    var formURL = $("#suspend_user_form").attr("action");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (resp) {
                if (resp.Success === "Y") {
                    $('#suspendUserConfirmModal').modal('hide');
                    showSuccessToast("User Suspended Successfully");
                    setTimeout(function () {
                        _loadUsers();
                    }, 500);
                } else {
                    //showErrorToast("user cannot be suspended");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
}
function initResetUserPwd() {
    var PartyId = $('input[id="resetPasswordForPartyId"]').val();
    var userLoginId = $('input[id="resetPasswordUserLoginId"]').val();
    var postData = { "partyId": App.xssValidate(PartyId), "userLoginId": App.xssValidate(userLoginId) };
    var formURL = getAppUrl("initResetUserPwd");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            beforeSend: function () {
                $('#resetPasswordUserConfirmModalResetPswdBtn').attr('disabled', true);
            },
            success: function (resp) {
                if (resp.Success === "Y") {
                    $('#resetPasswordUserConfirmModal').modal('hide');
                    showSuccessToast("Reset password initiated successfully, User will receive an e-mail with reset link");
                    setTimeout(function () {
                    }, 500);
                } else {
                    //showErrorToast("Not able to update password");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
}

function _reEnableUser() {
    var email = $("#userEmail").val()
    var postData = { email: App.xssValidate(email) };
    var formURL = getAppUrl("ajaxReenableOrgUser");
    console.log("Re-enabling user.. " + email);
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                showSuccessToast("User re-enabled successfully");
                setTimeout(function () {
                    location.href = getAppUrl("manage_users") + "?userReEnabled=Y"
                }, 200);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
}

function checkIfEmailExists() {
    console.log("checkemail invoked...")
    var email = $("#userEmail").val()
    var postData = { email: App.xssValidate(email) };
    var formURL = getAppUrl("checkEmailAlreadyExists");

    $("#email_notavailable").addClass("d-none");
    $("#removed_email_allow_reenable").addClass("d-none");

    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            dataType: "html",
            success: function (resp) {
                var respObj = JSON.parse(resp);
                if (respObj.EMAIL_EXISTS === "YES") {
                    if (respObj.IS_REMOVED_USER === "YES") {
                        $("#removed_email_allow_reenable").removeClass("d-none");
                        return true;
                    } else {
                        $("#email_notavailable").removeClass("d-none");
                        return false;
                    }
                } else {
                    //$("#emailInfo").html("FALSE");
                }
            },
            error: function (EMAIL_EXISTS) {
            }
        });
}

function updatePassword() {
    $('.loginFormSubmitBtn').on('click', function (evt) {
        let formData = {
            "PASSWORD": App.xssValidate($('#password').val()),
            "newPassword": App.xssValidate($('#newPassword').val()),
            "newPasswordVerify": App.xssValidate($('#newPasswordVerify').val())
        }
        $.ajax({
            url: "updatePassword",
            type: "POST",
            data: formData,
            success: function (resp) {
                renderError(resp);
            },
            error: function (resp) {
                renderError(resp);
            }
        });
    });
}

function renderError(res) {
    if (res.message == "success") {
        $('.toastMsgDiv').children().remove();
        if (typeof (res.info) == "object") {
            for (let i = 0; i < res.info.length; i++) {
                $('.toastMsgDiv').append(`<div class="alert alert-success">${res.info[i]}</div>`);
            }
        } else {
            $('.toastMsgDiv').append(`<div class="alert alert-success">${res.info}</div>`);
        }
        $('input:not(.loginFormSubmitBtn)').val('');
        $('.loginFormSubmitBtn').attr('disabled', true);
        setTimeout(function () {
            window.open('logout', '_self');
        }, 2500);

    } else if (res.message == "error") {
        $('.toastMsgDiv').children().remove();
        if (typeof (res.info) == "object") {
            for (let i = 0; i < res.info.length; i++) {
                $('.toastMsgDiv').append(`<div class="alert alert-danger">${res.info[i]}</div>`);
            }
        } else {
            $('.toastMsgDiv').append(`<div class="alert alert-danger">${res.info}</div>`);
        }
        $('input:not(.loginFormSubmitBtn)').val('');

    } else {
        console.log(res);
    }
}