import { App } from '../graphEditor/js/customJs/app.js';

$(function () {
    initiateUsersMgmtModals();
});


function initiateUsersMgmtModals() {
    $('#deleteUserConfirmModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var deletingPartyId = App.xssValidate(button.data('party-id')); // Extract info from data-* attributes
        var deletingPartyName = App.xssValidate(button.data('party-name'));
        if (deletingPartyName == null) deletingPartyName = "";

        var modal = $(this)
        modal.find('#deletePartyName').text(deletingPartyName);
        $("#deleteUser_partyId").val(deletingPartyId);
    })

    $('#suspendUserConfirmModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var suspendingPartyId = App.xssValidate(button.data('party-id'));
        var suspendUserPartyName = App.xssValidate(button.data('party-name'));
        if (suspendUserPartyName == null) suspendUserPartyName = "";

        var modal = $(this);
        modal.find('#suspendUserPartyName').text(suspendUserPartyName)
        $("#suspendUser_partyId").val(suspendingPartyId)
    });

    $('#activateUserConfirmModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var activateUserPartyId = App.xssValidate(button.data('party-id'));
        var activateUserPartyName = App.xssValidate(button.data('party-name'));
        if (activateUserPartyName == null) activateUserPartyName = "";

        var modal = $(this)
        modal.find('#activateUserPartyName').text(activateUserPartyName);
        $("#enableUser_partyId").val(activateUserPartyId)
    });
    $('#resetPasswordUserConfirmModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var resetPasswordForPartyId = App.xssValidate(button.data('party-id'));
        var resetPasswordForPartyName = App.xssValidate(button.data('party-name'));
        var resetPasswordUserLoginId = App.xssValidate(button.data('user-login-id'));
        if (resetPasswordForPartyName == null) resetPasswordForPartyName = "";

        var modal = $(this)
        modal.find('#resetPasswordForPartyName').text(resetPasswordForPartyName);
        modal.find('#resetPasswordForPartyId').val(resetPasswordForPartyId);
        modal.find('#resetPasswordUserLoginId').val(resetPasswordUserLoginId);
    });

    $('#password').blur(function () { checkPasswordPolicy('password', 'password_policy_error'); });

    $('#newPassword').blur(function () { checkPasswordPolicy('newPassword', 'password_policy_error'); });

    $('#newPasswordVerify').blur(function () { checkPasswordPolicy('newPasswordVerify', 'password_policy_error'); });
}

function checkPasswordPolicy(textFieldId, errorDivId) {
    var password = $('input[id="' + textFieldId + '"]').val();
    var postData = { password: App.xssValidate(password) };
    var formURL = getAppUrl("validatePasswordPolicy");
    $('#' + errorDivId).html("");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (resp) {
                if (resp._ERROR_MESSAGE_LIST_) {
                    //showErrorToast(resp._ERROR_MESSAGE_LIST_);
                    var errorMsgs = resp._ERROR_MESSAGE_LIST_;
                    var errorHtml = "";
                    for (var i = 0; i < errorMsgs.length; i++) {
                        errorHtml += "<div class=\"small text-danger p-1\"><i class=\"material-icons danger\">error</i> " + errorMsgs[i] + "</div>";
                    }
                    $('#' + errorDivId).html(errorHtml);
                }
            },
            error: function () {
                //TODO: handle error
            }
        });
}

export { initiateUsersMgmtModals };