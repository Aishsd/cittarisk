import { App } from '../graphEditor/js/customJs/app.js';

$(function () {

    // $.ajax({
    //     method: "POST",
    //     url: "isLoggedIn",
    //     data: { "sdid": '' },
    //     success: function (data) {
    //         if (!(typeof data == "string" && data.startsWith("<!DOCTYPE html>"))) {
    //             if (!data._IS_LOGGED_IN) {
    //                 window.location.href = 'login';
    //             }
    //             else {
    //                 if (data._IS_OTP_VERIFIED_) {
    //                     window.location.href = 'home';
    //                 } else {
    //                     // TODO: write all the OTP checking code here.
    //                 }
    //             }
    //         }
    //     },
    //     error: function (err) { console.log(err); }
    // });

    $('.resendOTPLink').hide(); $('#otp').hide(); $('.otpContinueBtn').attr('disabled', true);

    let urlParams = App.urlParams(true), status = (urlParams) ? urlParams.status : null, url = 'twofactorauthentication',
        isMaximumTryReached = (localStorage.isMaximumTryReached) ? localStorage.isMaximumTryReached : localStorage.isMaximumTryReached = false,
        maximumTryReachedTime = (localStorage.maximumTryReachedTime) ? localStorage.maximumTryReachedTime : localStorage.maximumTryReachedTime = new Date(),
        isOTPSent = (localStorage.isOTPSent) ? localStorage.isOTPSent : localStorage.isOTPSent = false,
        OTPSentTime = (localStorage.OTPSentTime) ? localStorage.OTPSentTime : localStorage.OTPSentTime = new Date();

    if (status && status === 'otpGenerated') {
        showOTP();
        if (isOTPSent == 'true') { localStorage.isMaximumTryReached = false; }
    }

    if (status && status !== null && (isMaximumTryReached == 'true' || isOTPSent == 'true')) {

        let time;
        if (isOTPSent == 'true') { time = OTPSentTime; status = 'otpSent'; }
        else { time = maximumTryReachedTime; }

        let dateTimeDifference = getDateTimeDifference(new Date(time), new Date(), true),
            seconds = dateTimeDifference.split(':')[2],
            minutes = dateTimeDifference.split(':')[1],
            hours = dateTimeDifference.split(':')[0];

        // console.log(dateTimeDifference); console.log(status)

        if (status == 'otpSent' && Math.sign(hours) >= 0 && Math.sign(minutes) >= 0 && Math.sign(seconds) >= 0) {
            startTimer(time, true);
            setTimeout(function () {
                localStorage.OTPSentTime = new Date(new Date(time).getTime() - 1000);
            }, 1000);
        }

        if (status == 'otpGenerated' && Math.sign(seconds) > 0 && minutes <= 15) {
            startTimer(time, true);
            setTimeout(function () {
                localStorage.maximumTryReachedTime = new Date(new Date(time).getTime() - 1000);
            }, 1000);
            $('.otpDiv').hide(); $('.resendOTPLink').hide();
            addToastMsg('Maximum tries reached, Please wait...', 'info', '.checkBoxes');
        }

    }

    $('#selectAuth').submit(function (e) {
        e.preventDefault();
        let checkedValue = $("input[name='inlineRadioOptions']:checked").val();

        if (checkedValue) {
            $('.alert').remove();
            $.ajax({
                url: 'generateOtp',
                method: "POST",
                data: { "value": checkedValue },
                beforeSend: function () {
                    $('.sendOtpBtn').text('Sending');
                    $('.sendOtpBtn').attr("disabled", true);
                    $('.link').slideUp(800);
                },
                success: function (res) {
                    console.log(res)

                    if (res.data.message == 'success') {
                        let hours = 24;
                        localStorage.isOTPSent = isOTPSent = true; localStorage.OTPSentTime = new Date(new Date().getTime() + 60 * 60 * hours * 1000);

                        $('.sendOtpBtn').slideUp(800);
                        addToastMsg('OTP Sent', 'success', '.checkBoxes');

                        setTimeout(function () { window.location.href = `${url}?${App.encrypt('status=otpGenerated')}`; }, 2500);
                    }
                    else if (res.data.message == 'error') {
                        $('.link').slideDown(800);
                        $('.sendOtpBtn').text('Try Again'); addToastMsg('Failed to send OTP', 'danger', '.checkBoxes');
                    }
                },
                error: function (res) {
                    console.log(res);
                    $('.link').slideDown(800);
                    $('.sendOtpBtn').text('Try Again'); addToastMsg('Failed to send OTP, Please try again later', 'danger', '.checkBoxes');
                },
            });
        }
        else {
            addToastMsg('Please select any one option', 'danger', '.checkBoxes');
        }

    });

    $('.resendOTPLink').on('click', function () {
        window.location.href = url;
    });

    $('.otpInput').on('keyup', function (e) {
        let otp = $('.otpInput').val();
        if (otp && otp.length >= 4) {
            $('.otpContinueBtn').attr('disabled', false);
        } else {
            $('.otpContinueBtn').attr('disabled', true);
        }
    });

    let count = 3; // only 3 times submit otp

    $("#otp").submit(function (e) {
        e.preventDefault();
        count--;
        $('.triesLeft').text(`Note: ${count} tries left`);
        let otp = $('.otpInput').val();
        if (count > 0) {
            if (otp && otp.length >= 4) {
                $.ajax({
                    url: 'verifyOTP',
                    method: "POST",
                    data: { "otpValue": otp },
                    beforeSend: function () {
                        $('.otpContinueBtn').text('Validating');
                        $('.otpContinueBtn').attr('disabled',true);
                    },
                    success: function (res) {
                        console.log(res);
                        // res = 'successs'

                        if (res.data.message == 'success') {
                            $('.triesLeft').text('');
                            $('.otpContinueBtn').slideUp(800);

                            addToastMsg('OTP Verification successful', 'success', '.otpContainer');

                            localStorage.OTPSentTime = localStorage.maximumTryReachedTime = localStorage.isOTPSent = localStorage.isMaximumTryReached = null;
                            // GOTO home page
                            setTimeout(function () { window.location.href = 'home'; }, 2500);
                        } else {
                            if (res.data.message == 'error') {
                                $('.otpContinueBtn').attr('disabled', false);
                                $('.otpContinueBtn').text('Re-try');
                                addToastMsg('Invalid OTP. Please check your code and try again.', 'danger', '.otpContainer');
                                $('.otpInput').val('');
                            }
                        }
                    },
                    error: function (res) {
                        console.log(res);
                        $('.otpContinueBtn').text('Try Again');
                        $('.otpContinueBtn').attr('disabled',false);
                        addToastMsg('Server Failed', 'danger', '.otpContainer');
                    },
                });
            }
        } else {
            let minutes = 15;
            localStorage.isMaximumTryReached = true; localStorage.isOTPSent = false;
            localStorage.maximumTryReachedTime = new Date(new Date().getTime() + minutes * 60000);
            addToastMsg('Maximum try reached, please try again after 15 minutes', 'warning', '.otpContainer');
            $('.triesLeft').text("");
            $('.timer').hide(); $('.resendOTPLink').hide();
            $('.otpContainer').hide(); $('.otpContinueBtn').hide();
            $('.link').text('Reload Page'); $('.link').show();
        }
    });

});

function addToastMsg(message, type, place) {
    $('.text').remove();
    let alertDiv, icon;
    if (type === 'danger') { icon = `close` }
    else if (type === 'success') { icon = `check`; }
    else if (type === 'warning') { icon = `warning`; }
    else if (type === 'info') { icon = `fa-info-circle` }

    alertDiv = `<div class="text text-${type} p-1"><i class="fa fa-${icon}" aria-hidden="true"></i>&nbsp;${message}</div>`;
    $(place).after(alertDiv);
}

function startTimer(expiryTime, reload) {
    $('.timer').show(); $('.timer').text('');
    var timer2;

    if (expiryTime) {
        timer2 = getDateTimeDifference(new Date(), new Date(expiryTime), false);
    } else {
        timer2 = '24:00:00';
    }

    var interval = setInterval(function () {
        var timer = timer2.split(':');
        var hours = parseInt(timer[0], 10);
        var minutes = parseInt(timer[1], 10);
        var seconds = parseInt(timer[2], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        hours = (minutes < 0) ? --hours : hours;

        if (hours < 0 && minutes < 0) {
            if (reload) {
                localStorage.isMaximumTryReached = false; localStorage.isOTPSent = true;
                window.location.reload();
            }
            else { clearInterval(interval); $('.text').remove(); resetForm(); }
        }

        seconds = (seconds < 0) ? 59 : seconds; seconds = (seconds < 10) ? '0' + seconds : seconds;
        minutes = (minutes < 0) ? 59 : minutes; minutes = (minutes < 10) ? '0' + minutes : minutes;

        $('.timer').html(hours + ':' + minutes + ':' + seconds);
        timer2 = hours + ':' + minutes + ':' + seconds;
    }, 1000);
}

function resetForm() {
    $('.timer').hide();
    const chbx = document.getElementsByName("inlineRadioOptions");
    for (let i = 0; i < chbx.length; i++) { chbx[i].checked = false; }
    $('.checkBoxes').slideDown(800);
}

function showOTP() {
    $('.checkBoxes').slideUp(800);
    $('.sendOtpBtn').hide(800);
    $('#otp').slideDown();
    $('.link').hide(); $('.resendOTPLink').show();
}

function getDateTimeDifference(date1, date2, isSignRequired) {
    var res = (date1 - date2) / 1000;

    if (!isSignRequired) {
        res = Math.abs(res);
    } // else condn: Sign is required

    // get total days between two dates
    var days = Math.floor(res / 86400);

    // get hours        
    var hours = Math.floor(res / 3600) % 24;

    // get minutes
    var minutes = Math.floor(res / 60) % 60;

    // get seconds
    var seconds = res % 60;

    // console.log(hours + ':' + minutes + ':' + seconds);
    return (hours + ':' + minutes + ':' + seconds);
} 