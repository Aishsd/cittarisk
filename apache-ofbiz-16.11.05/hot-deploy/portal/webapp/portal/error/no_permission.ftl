

<div class="container-fluid">

    <div class="danger">

    </div>
    <h3></h3>

    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading"><i class="material-icons">error_outline</i> Unauthorized!</h4>

        <p>You do not have permission to access this page.</p>
    </div>

</div>