

<div class="container-fluid">
    <div class="m-2">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading"><i class="material-icons" style="font-size:25px;">error_outline</i> Not Allowed to add more users!</h4>

            <p>You have reached the max users limit for this subscription!</p>
        </div>
    </div>
</div>