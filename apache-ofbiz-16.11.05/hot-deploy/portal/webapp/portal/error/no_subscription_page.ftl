

<div class="container-fluid">

    <div class="danger">

    </div>
    <h3></h3>

    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading"><i class="material-icons">error_outline</i> Not Subscribed!</h4>

        <p>You do not have a valid subscription to access the Portal.</p>
    </div>

    <div>
        <a class="btn btn-outline-primary" href="<@ofbizUrl>logout</@ofbizUrl>"> <i class="material-icons">
                keyboard_backspace
            </i> Back to Login</a>
        <a class="btn btn-outline-primary" href="javascript:void();" onclick="location.reload()">
            <i class="fa fa-refresh" aria-hidden="true"></i> Retry</a>
    </div>

</div>