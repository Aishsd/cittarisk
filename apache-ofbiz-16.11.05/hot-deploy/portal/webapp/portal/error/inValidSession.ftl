<!DOCTYPE html>
<!-- Begin Screen component://common/widget/CommonScreens.xml#ajaxNotLoggedIn -->
<!-- Begin Screen component://common/widget/CommonScreens.xml#AjaxGlobalDecorator -->
<!-- Begin Template component://common/template/includes/HtmlHeaderForAjax.ftl -->
<html lang="en-US" dir="ltr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<!-- End Template component://common/template/includes/HtmlHeaderForAjax.ftl -->
<span size="14pt" class="message">Your session has expired. A login is required. You can refresh the page or save your data to login.</span>
<!-- Begin Template component://common/template/includes/HtmlFooterForAjax.ftl -->
</body>
</html>
<!-- End Template component://common/template/includes/HtmlFooterForAjax.ftl -->
<!-- End Screen component://common/widget/CommonScreens.xml#AjaxGlobalDecorator -->
<!-- End Screen component://common/widget/CommonScreens.xml#ajaxNotLoggedIn -->