import org.apache.ofbiz.base.util.Debug
import org.apache.ofbiz.base.util.UtilMisc
import org.apache.ofbiz.base.util.UtilProperties
import org.apache.ofbiz.base.util.UtilValidate
import org.apache.ofbiz.entity.GenericValue
import org.apache.ofbiz.service.ServiceUtil

String productStoreId = UtilProperties.getPropertyValue("subscription.properties","autopatt.product.store", "AUTOPATT_STORE");
String PORTAL_HOST_URL = UtilProperties.getPropertyValue("portal.properties","autopatt.portal.otp", "https://localhost:8443/portal/c/twofactorauthentication?status=otpGenerated");

Debug.logInfo("-=-=-=- Sending Employee password reset Email -=-=-=-", "")
result = ServiceUtil.returnSuccess()
String module = "PortalOTPSendingToMail.groovy"

String employeePartyName = context.employeePartyName
String employeeEmail = context.employeeEmail
String otp = context.otp


if (employeeEmail) {
    Debug.logInfo("----- Sending email to: $employeeEmail -----", module)

    String emailType = "ORG_MFA"
    GenericValue productStoreEmailSetting = delegator.findOne("ProductStoreEmailSetting",
            UtilMisc.toMap("productStoreId",productStoreId, "emailType", emailType), false)

    if(UtilValidate.isNotEmpty(productStoreEmailSetting)) {
        Map bodyParameters = UtilMisc.toMap()
        bodyParameters.put("employeeEmail", employeeEmail)
        bodyParameters.put("otp", otp)
        bodyParameters.put("employeePartyName", employeePartyName)
        bodyParameters.put("PORTAL_HOST_URL", PORTAL_HOST_URL)

        Debug.logInfo("Body Parameters: " + bodyParameters, module);

        dispatcher.runSync("sendMailFromScreen",
                UtilMisc.toMap(
                        "sendTo", employeeEmail,
                        "sendFrom", productStoreEmailSetting.getString("fromAddress"),
                        "subject", productStoreEmailSetting.getString("subject"),
                        "bodyScreenUri", productStoreEmailSetting.getString("bodyScreenLocation"),
                        "bodyParameters", bodyParameters));

        result.successMessage = (String) "Email Sent to [$employeeEmail] "
        result.result = "Email Sent"
    } else  {
        return ServiceUtil.returnFailure("No Product store email setting found")
    }

} else {
    Debug.logError("Got no SendTo email id", module)
    return ServiceUtil.returnFailure("Got no SendTo email id")
}

return result
