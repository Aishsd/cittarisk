
var admins = {
    removeUser: _removeUser
};

function unescapeHtmlText(text) {
    text = jQuery('<div />').html(text).text();
    if (text.replace(/\s/g, '').length) {
        return text;
    }
}

$(function () {
    console.log("loggedInUserName" + $('.userName').text());
    if ($('.userName').text()) {
        $.ajax({
            method: "POST",
            url: "isLoggedIn",
            data: { "sdid": '' },
            success: function (data) {
                console.log("_IS_LOGGED_IN::" + typeof data == "string");
                if (!(typeof data == "string" && data.startsWith("<!DOCTYPE html>"))) {
                    if (!data._IS_LOGGED_IN) {
                        console.log("done");
                        window.location.href = 'login.ftl';
                    }
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    updatePassword();
    
    checkSidebarAndNavbarIsCollapsed();

});

function checkSidebarAndNavbarIsCollapsed() {
    setTimeout(function () {
        if ($("#wrapper").hasClass('collapse')) {
            $('.sidebar-heading').addClass('collapse');
            $('.sidebar-heading').hide();
            $('.sidebarMenuList').removeClass('pt-0');
        } else {
            $('.sidebar-heading').removeClass('collapse');
            $('.sidebar-heading').show();
            $('.sidebarMenuList').addClass('pt-0');
        }
    }, 200)

    $('#menu-toggle').on('click', function () {
        if (!$("#wrapper").hasClass('collapse')) {
            $('.sidebar-heading').removeClass('collapse');
            $('.sidebar-heading').hide();
            $('.sidebarMenuList').removeClass('pt-0');
        } else {
            $('.sidebar-heading').addClass('collapse');
            $('.sidebar-heading').show();
            $('.sidebarMenuList').addClass('pt-0');
        }
    });
}

function _removeUser() {
    var userPartyId = unescapeHtmlText($("#deleteAdminUser_partyId").val());
    console.log("deleting " + userPartyId)
    var postData = { adminPartyId: userPartyId };
    var formURL = $("#delete_admin_user_form").attr("action");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                $('#deleteAdminUserConfirmModal').modal('hide');
                showSuccessToast("Admin User Deleted Successfully");
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
}

// Check if email already exists for admin user
function checkIfAdminEmailExists() {
    var email = unescapeHtmlText($("#userEmail").val())
    var postData = { email: email };
    var formURL = getUrl("checkIfEmailAlreadyExists");
    $("#email_notAvailable").addClass("d-none");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (resp) {
                if (resp.EMAIL_EXISTS === "YES") {
                    $("#email_notAvailable").removeClass("d-none");
                } else {
                }
            },
            error: function (EMAIL_EXISTS) {
            }
        });
}
function checkIfOrgIdExists() {
    var tenantId = unescapeHtmlText($("#organizationId").val())
    var postData = { tenantId: tenantId };
    var formURL = getUrl("checkIfOrgIdAlreadyExists");
    $("#orgId_notAvailable").addClass("d-none");
    $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (resp) {
                console.log("id")
                if (resp.ORGID_EXISTS === "YES") {
                    $("#orgId_notAvailable").removeClass("d-none");
                } else {
                }
            },
            error: function (EMAIL_EXISTS) {
            }
        });
}

function restrictSpecialCharacters(event) {
    var regex = new RegExp("^[a-zA-Z0-9_-]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function updatePassword() {
    $('.changePasswordFormSubmitBtn').on('click', function (evt) {
        let formData = {
            "PASSWORD": unescapeHtmlText($('#password').val()),
            "newPassword": unescapeHtmlText($('#newPassword').val()),
            "newPasswordVerify": unescapeHtmlText($('#newPasswordVerify').val())
        }
        $.ajax({
            url: "updatePassword",
            type: "POST",
            data: formData,
            success: function (resp) {
                renderError(resp);
            },
            error: function (resp) {
                renderError(resp);
            }
        });
    });
}

function renderError(res) {
    if (res.message == "success") {
        $('.toastMsgDiv').children().remove();
        if (typeof (res.info) == "object") {
            for (let i = 0; i < res.info.length; i++) {
                $('.toastMsgDiv').append(`<div class="alert alert-success">${res.info[i]}</div>`);
            }
        } else {
            $('.toastMsgDiv').append(`<div class="alert alert-success">${res.info}</div>`);
        }
        $('input:not(.changePasswordFormSubmitBtn)').val('');
        $('.changePasswordFormSubmitBtn').attr('disabled', true);
        setTimeout(function () {
            window.open('logout', '_self');
        }, 2500);

    } else if (res.message == "error") {
        $('.toastMsgDiv').children().remove();
        if (typeof (res.info) == "object") {
            for (let i = 0; i < res.info.length; i++) {
                $('.toastMsgDiv').append(`<div class="alert alert-danger">${res.info[i]}</div>`);
            }
        } else {
            $('.toastMsgDiv').append(`<div class="alert alert-danger">${res.info}</div>`);
        }
        $('input:not(.changePasswordFormSubmitBtn)').val('');

    } else {
        console.log(res);
    }
}


//client deletion
function deleteTenant(tenantIdValue) {
    bootbox.confirm({
        title: "Client Organisation Deletion",
        message: "Please confirm to delete client organisation",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    method: "POST",
                    url: "deleteClient",
                    data: { "tenantId": tenantIdValue },
                    success: function (res) {
                        let loadingBootbox = bootbox.dialog({
                            message: `<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>  Loading...</p>`,
                            closeButton: false,
                        });

                        setTimeout(function () {
                            loadingBootbox.modal('hide');
                            bootbox.dialog({
                                message: `<span class="text-center mb-0">Client organisation deleted successfully.</span>`,
                                buttons: {
                                    cancel: {
                                        label: 'Close',
                                        //label : Close,
                                        callback: function () {
                                            window.location.reload();
                                        }
                                    }
                                }
                            });
                        }, 1500);
                        // window.location.reload();
                    },
                    error: function (err) {
                        setTimeout(function () {
                            loadingBootbox.modal('hide');
                            bootbox.dialog({
                                message: `<span class="text-center mb-0">Client organisation deletion failed.</span>`,
                                buttons: {
                                    cancel: {
                                        label: 'Close'
                                    }
                                }
                            });
                        }, 1500);
                    }
                });
            }
        }
    });
}