var App = {
    urlParams: function () {
        let search = App.xssValidate(location.search.substring(1)), urlParams;

        if (search && (search.includes('psid') || search.includes('tagid') || search.includes('status'))) {
            try {
                urlParams = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', function (key, value) { return key === "" ? value : decodeURIComponent(value) });
                return urlParams;
            } catch (error) {
                console.log(error);
                return {};
            }
        } else {
            try {
                search = this.decrypt(search.replace("%3D", "="));
                urlParams = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', function (key, value) { return key === "" ? value : decodeURIComponent(value) });
                return urlParams;
            } catch (error) {
                console.log(error);
                return {};
            }
        }
    },
    xssValidate: function (text) {
        text = jQuery('<div />').html(text).text();
        if (text.replace(/\s/g, '').length) {
            return text;
        }
    },
    getDate: function (timeStamp) {
        let date, currentDate, month;

        (timeStamp) ?
            date = new Date(timeStamp) : date = new Date();

        currentDate = date.getDate(); (currentDate < 10) ? currentDate = `0${currentDate}` : currentDate = `${currentDate}`;
        month = date.getMonth() + 1; (month < 10) ? month = `0${month}` : month = `${month}`;

        return {
            date: date,
            localeDateIndia: `${currentDate}/${month}/${date.getFullYear()}, ${date.toLocaleString('en-IN', {
                hour: 'numeric', minute: "numeric", second: "numeric", hour12: false
            }).toLocaleUpperCase()}`,
            localeDate: `${month}/${currentDate}/${date.getFullYear()}, ${date.toLocaleString('en-IN', {
                hour: 'numeric', minute: "numeric", second: "numeric", hour12: false
            }).toLocaleUpperCase()}`,
        }
    },
}

$(function () {

    $('#otp').hide();
    $('.otpContinueBtn').attr('disabled', true);

    let urlParams = App.urlParams(), status = (urlParams) ? urlParams.status : null;
    console.log(status);

    if (status && status === 'otpGenerated') {
        showOTP();
    }

    $('#selectAuth').submit(function (e) {
        e.preventDefault();
        let checkedValue = $("input[name='inlineRadioOptions']:checked").val();

        if (checkedValue) {
            $('.alert').remove();
            $.ajax({
                url: '',
                data: { "value": checkedValue },
                beforeSend: function () {
                    $('.sendOtpBtn').text('Sending');
                },
                success: function (res) {
                    console.log(res)

                    // Add timer
                    startTimer(); //TODO: add this inside condition

                    res = 'success';
                    if (res == 'success') { //.data.message == 'success') {
                        // IF API response is successful
                        addToastMsg('OTP Sent', 'success');
                        showOTP();
                    }
                },
                error: function (res) {
                    console.log(res)
                    addToastMsg('Failed to send OTP', 'danger');
                },
            });
        }
        else {
            console.log('Nothing is checked!');
            addToastMsg('Please select any one option', 'danger');
        }

    });

    $('.otpInput').on('keyup', function (e) {
        let otp = $('.otpInput').val();
        if (otp && otp.length == 4) {
            $('.otpContinueBtn').attr('disabled', false);
        } else {
            $('.otpContinueBtn').attr('disabled', true);
        }
    });

    let count = 0; // only 3 times submit otp //TODO: later 

    $("#otp").submit(function (e) {
        e.preventDefault();
        count++;
        let otp = $('.otpInput').val();

        console.log(count);
        if (count <= 3) {
            if (otp && otp.length == 4) {
                $.ajax({
                    url: '',
                    data: { "otpValue": otp },
                    beforeSend: function () {
                        $('.otpContinueBtn').text('Validating');
                    },
                    success: function (res) {

                        console.log(res);
                        res = 'successs'

                        // IF API response is successful
                        if (res == 'success') { //.data.message == 'success') {
                            $('.otpContinueBtn').slideUp(800);
                            addToastMsg('OTP Verification successful', 'success');

                            // GOTO home page
                            setTimeout(function () { window.location.href = 'home'; }, 1500);
                        } else {
                            // if (res == 'error') { //.data.message == 'error') {
                            $('.otpContinueBtn').attr('disabled', true);
                            $('.otpContinueBtn').text('Re-try');
                            addToastMsg('Incorrect OTP', 'danger');
                            $('.otpInput').val('');
                            // }
                        }
                    },
                    error: function (res) {
                        console.log(res)
                        addToastMsg('Server Failed', 'danger');
                    },
                });
            }
        } else {
            addToastMsg('Maximum try reached, please try again after 15 minutes', 'warning');
            $('.timer').hide();
            $('.otpInput').hide();
            $('.otpContinueBtn').hide();
        }
    });

});

function addToastMsg(message, type) {
    $('.alert').remove();
    let alertDiv = `<div class="alert alert-${type}" role="alert">${message}</div>`;
    $('.checkBoxes').after(alertDiv);
}

function startTimer(expiryTime) {
    $('.timer').show(); $('.timer').text('');
    var timer2;

    // TODO: get expiryTime from db and minus with current time

    // expiryTime = App.getDate(1588591888823).localeDate.replace(' ', '').split(',')[1];

    expiryTime = 1588591888823;

    if (expiryTime) {
        timer2 = getDateTimeDifference(new Date(), new Date(expiryTime));
    } else {
        timer2 = '03:01';
    }

    var interval = setInterval(function () {
        var timer = timer2.split(':');
        var hours = parseInt(timer[0], 10);
        var minutes = parseInt(timer[1], 10);
        var seconds = parseInt(timer[2], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        hours = (minutes < 0) ? --hours : hours;

        if (hours < 0 && minutes < 0) {
            clearInterval(interval);
            $('.alert').remove();
            resetForm();
            $('.sendOtpBtn').text('Resend OTP');
            $('.sendOtpBtn').show();
        }

        seconds = (seconds < 0) ? 59 : seconds; seconds = (seconds < 10) ? '0' + seconds : seconds;
        minutes = (minutes < 0) ? 59 : minutes; minutes = (minutes < 10) ? '0' + minutes : minutes;

        $('.timer').html(hours + ':' + minutes + ':' + seconds);
        timer2 = hours + ':' + minutes + ':' + seconds;
    }, 1000);
}

function resetForm() {
    // $('#otp').hide(); $('#otpContinueBtn').hide();
    $('.timer').hide();
    const chbx = document.getElementsByName("inlineRadioOptions");
    for (let i = 0; i < chbx.length; i++) { chbx[i].checked = false; }
    $('.checkBoxes').slideDown(800);
}

function showOTP() {
    $('.checkBoxes').slideUp(800);
    $('.sendOtpBtn').hide(800);
    $('#otp').slideDown();
    $('.otpHasLink').hide();
}

function getDateTimeDifference(date1, date2) {

    var res = Math.abs(date1 - date2) / 1000;

    // get total days between two dates
    var days = Math.floor(res / 86400);

    // get hours        
    var hours = Math.floor(res / 3600) % 24;

    // get minutes
    var minutes = Math.floor(res / 60) % 60;

    // get seconds
    var seconds = res % 60;

    return (hours + ':' + minutes + ':' + seconds);
}