<#if requestAttributes.errorMessageList?has_content>
    <#assign errorMessageList=requestAttributes.errorMessageList>
</#if>
<#if requestAttributes.eventMessageList?has_content>
    <#assign eventMessageList=requestAttributes.eventMessageList>
</#if>
<#if requestAttributes.serviceValidationException??>
    <#assign serviceValidationException=requestAttributes.serviceValidationException>
</#if>
<#if requestAttributes.uiLabelMap?has_content>
    <#assign uiLabelMap=requestAttributes.uiLabelMap>
</#if>

<#if !errorMessage?has_content>
    <#assign errorMessage=requestAttributes._ERROR_MESSAGE_!>
</#if>
<#if !errorMessageList?has_content>
    <#assign errorMessageList=requestAttributes._ERROR_MESSAGE_LIST_!>
</#if>
<#if !eventMessage?has_content>
    <#assign eventMessage=requestAttributes._EVENT_MESSAGE_!>
</#if>
<#if !eventMessageList?has_content>
    <#assign eventMessageList=requestAttributes._EVENT_MESSAGE_LIST_!>
</#if>

<div class="container-fluid">

    <div class="login-sidenav">
        <#include "../common/preauth_logo.ftl" />
        <#--  <div class="login-main-text">
            <h2>AutoPatt Console</h2>
            <p>Forgot Password initiated successfully</p>
        </div>  -->
    </div>
    <div class="login-main">
        <div class="login-form">
            <div class="login-main-text">
                <h2>AutoPatt Console</h2>
                <p>Forgot Password initiated successfully</p>
            </div>
            <div>
                <hr />
            </div>
            <h3>Forgot Password </h3>
            <br />
            <div>
                <#list errorMessageList as error>
                    <div class="alert alert-danger" role="alert">
                        ${error}
                    </div>
                </#list>
            </div>
            <p>An email has been sent to your email address with a link to reset your password.
                <p />

                <#--<a href="<@ofbizUrl>spwd?token=${requestAttributes.TOKEN!}</@ofbizUrl>">Click here to reest
                    password</a>
                    <br />
                    <i>Link above will be available in the email, this link will go away once we implement email
                        service</i>-->
        </div>
    </div>
</div>