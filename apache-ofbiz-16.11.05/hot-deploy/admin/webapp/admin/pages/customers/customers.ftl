
<div class="container-fluid">

    <div class="table-title">
        <div class="row">
            <div class="col-sm-5">
                <h4>Clients</h4>
            </div>
            <div class="col-sm-7">
                <a href="<@ofbizUrl>new_customer</@ofbizUrl>" class="btn btn-primary" title="Onboard New Customer"><i class="material-icons">&#xE147;</i> <span>New Client</span></a>
                <a href="<@ofbizUrl>customers</@ofbizUrl>" class="btn btn-primary" title="Refresh"><i class="material-icons">refresh</i> <span>Refresh</span></a>
                <a href="<@ofbizUrl>in-progress</@ofbizUrl>" class="btn btn-info" title="Refresh"><i class="material-icons">update</i> <span>View Transactions</span></a>
            </div>
        </div>
    </div>

    <#if requestParameters.createInitiated?? && requestParameters.createInitiated=="Y">
        <div class="alert alert-success" role="alert">
            <i class="material-icons">check</i> Customer creation process has been initiated.... <a href="<@ofbizUrl>view_transaction</@ofbizUrl>?transactionId=${requestParameters.transactionId!}">View Progress</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </#if>

    <div class="table-content">
        <table id="customerTab" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Organization Id</th>
                <th>Organization Name</th>
                <th>Subscription</th>
                <th>Created On</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <#if customers?? && customers?size &gt; 0 >
                <#list customers as org>
                    <tr>
                        <td>${org_index + 1}</td>
                        <td class="`tenantID`">${org.tenantId!}</td>
                        <td><a href="<@ofbizUrl>manage_customer?orgPartyId=${org.orgPartyId!}</@ofbizUrl>">
                                ${Static["org.apache.ofbiz.party.party.PartyHelper"].getPartyName(delegator, org.orgPartyId!, false)}
                            </a>
                        </td>
                        <td>
                            <#if org.hasActiveSubscription?? && org.hasActiveSubscription>
                                <span class="status text-success" >&bull;</span>
                                <span>Active
                                    <span class="small text-muted">
                                        (
                                        <#list org.subscribedProductIds as prodId>
                                            ${prodId!}
                                            <#if prodId_has_next>, </#if>
                                        </#list>
                                        )</span>
                                </span>
                            <#else>
                                <span class="status text-danger" >&bull;</span> <span>No Active Subscriptions</span>
                            </#if>
                        </td>
                        <td>${org.createdStamp!?date}</td>
                        <td width="20%">
                            <a href="<@ofbizUrl>manage_customer?orgPartyId=${org.orgPartyId!}</@ofbizUrl>" class="btn btn-outline-primary" title="Edit" data-toggle="tooltip"><i class="material-icons">edit</i></a>
                            <button type="button" class="btn btn-outline-danger tenantDelete" id="modalConfirmDeleteLabel"  onclick="deleteTenant('${org.tenantId!}')" title="Delete"><i class="material-icons">delete</i></button>
                        </td>
                        <!-- Modal -->
                            <div class="modal fade" id="modalConfirmDeleteLabel" tabindex="-1" role="dialog" aria-labelledby="tenantDeleteModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="tenantDeleteModalLabel">Organization / Client Deletion</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    Are you sure want to delete...?
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button id="deleteTenant" class="btn btn-primary">Delete</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                    </tr>
                </#list>
            <#else>
                <tr>
                    <td colspan="10">No customers onboarded yet.</td>
                </tr>
            </#if>


            </tbody>
        </table>

    </div>




</div>