package com.autopatt.admin.constants;

public interface UserStatusConstants {
    String ACTIVE = "ACTIVE";
    String INACTIVE = "INACTIVE";
    String LOCKED = "LOCKED";
    String SUSPENDED = "SUSPENDED";
}
