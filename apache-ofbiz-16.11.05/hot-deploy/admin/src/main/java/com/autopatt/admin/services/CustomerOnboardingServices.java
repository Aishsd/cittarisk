/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.autopatt.admin.services;

import com.autopatt.admin.constants.SecurityGroupConstants;
import com.autopatt.admin.utils.NewTenantTransactionLogUtils;
import com.autopatt.admin.utils.TenantCommonUtils;
import com.autopatt.admin.utils.UserLoginUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.ofbiz.base.util.*;
import org.apache.ofbiz.entity.*;
import org.apache.ofbiz.entity.transaction.TransactionUtil;
import org.apache.ofbiz.entity.util.EntityQuery;
import org.apache.ofbiz.entity.util.EntityUtilProperties;
import org.apache.ofbiz.party.party.PartyHelper;
import org.apache.ofbiz.service.*;
import org.codehaus.plexus.util.FastMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.*;

/**
 * Services for Customer Onboarding
 * Tenant Creation
 * Org Setup
 */
public class CustomerOnboardingServices {

    public static final String module = CustomerOnboardingServices.class.getName();
    private static Properties ONBOARDING_PROPERTIES = UtilProperties.getProperties("onboarding.properties");

    /**
     * Onboard a new Customer.
     * Creates a new tenant DB.
     * Creates Org Party record
     * Creates First User (admin) for tenant
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> onboardNewCustomer(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Delegator delegator = ctx.getDelegator();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String tenantId = (String) context.get("tenantId");
        String organizationName = (String) context.get("organizationName");

        String contactFirstName = (String) context.get("contactFirstName");
        String contactLastName = (String) context.get("contactLastName");
        String contactEmail = (String) context.get("contactEmail");
        String contactPassword = (String) context.get("contactPassword");
        String sendNotificationToContact = (String) context.get("sendNotificationToContact");

        String newTenantTransactionId = (String) context.get("transactionId");

        // TODO: Validation - check for duplicate tenantId

        if(UtilValidate.isEmpty(tenantId)) {
            // Generate tenantId from organizationName
            tenantId = RandomStringUtils.random(6, "abcdefghijklmnopqrstuvwyz1234567890_".toCharArray());
        }
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "INITIATED", "Initiating Onboarding process", "Beginning the onboarding process");

        // 1. Initiate Tenant DB Creation
        String dbHostIp = ONBOARDING_PROPERTIES.getProperty("onboarding.database.mysql.hostname", "127.0.0.1");
        String dbHostPort = ONBOARDING_PROPERTIES.getProperty("onboarding.database.mysql.port", "3306");
        String dbUsername = ONBOARDING_PROPERTIES.getProperty("onboarding.database.mysql.username");
        String dbPassword = ONBOARDING_PROPERTIES.getProperty("onboarding.database.mysql.password");

        String tenantDbPrefix = tenantId;
        // Remove special chars from tenantDatabaseName
        tenantDbPrefix = tenantDbPrefix.replaceAll("[^\\w]","");
        String tenantDbUsername = "user" + tenantDbPrefix;
        String tenantDbPassword = "P@" + RandomStringUtils.random(15, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwyz1234567890@".toCharArray());

        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS", "Creating Databases", "beginning to create tenant databases");
        boolean isSuccess = createTenantDatabase(dispatcher, newTenantTransactionId, dbHostIp, dbHostPort, dbUsername, dbPassword, tenantDbPrefix, tenantDbUsername, tenantDbPassword);
        if(!isSuccess) {
            Debug.logError("Error running script to create tenant database for tenant " + tenantId, module);
            NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED", "Creating Databases", "error running script to create tenant databases");
            return ServiceUtil.returnFailure("Error running script to create tenant Database for tenant " + tenantId);
        }

        // Create Tenant entries
        Debug.logInfo("Creating Tenant and TenantDataSource entries ", module);
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS", "Creating Tenant Entries", "beginning to create tenant entries in main db");
        try {
            Map<String,Object> createTenantResp = dispatcher.runSync("createTenantEntries", UtilMisc.<String, Object> toMap("tenantId", tenantId,
                    "organizationName", organizationName,
                    "dbHostIp",dbHostIp,
                    "dbHostPort",dbHostPort,
                    "tenantDbPrefix",tenantDbPrefix,
                    "tenantDbUsername",tenantDbUsername,
                    "tenantDbPassword",tenantDbPassword,
                    "userLogin", userLogin));
            if(!ServiceUtil.isSuccess(createTenantResp)) {
                Debug.logError("createTenantEntries service failed to create Tenant and TenantDataSource entries in Main DB", module);
                Debug.logError("Response: " + createTenantResp, module);
                NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED",
                        "Creating Tenant Entries",
                        "createTenantEntries service failed to create Tenant and TenantDataSource entries for new tenant. Details: " + createTenantResp.get("message"));
                return ServiceUtil.returnFailure("Unable to create Tenant and TenantDataSource entries for new tenant " + tenantId);
            }
        } catch (GenericServiceException e) {
            Debug.logError("Unable to create Tenant and TenantDataSource entries Main DB", module);
            e.printStackTrace();
            NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED",
                    "Creating Tenant Entries", ExceptionUtils.getFullStackTrace(e));
            return ServiceUtil.returnFailure("Error Creating Tenant Entries for new tenant " + tenantId);
        }
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                "Done Creating Tenant Entries", "Tenant entries in main db created successfully");

        // 2. Create OrgEntry in Main DB (along with status)
        Debug.logInfo("Creating OrgEntry in Main Db................", module);
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                "Creating Org Party (Main DB)", "Beginning to create org party in Main DB");
        String mainDbOrgPartyId = null;
        try {
            mainDbOrgPartyId = createOrgPartyInMainDB(dispatcher, userLogin, tenantId, organizationName);
        } catch (OrgPartyCreationException e) {
            e.printStackTrace();
            NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED",
                    "Creating Org Party (Main DB)", "Creating org party in main db failed. Details: " + ExceptionUtils.getFullStackTrace(e));
            return ServiceUtil.returnFailure(e.getMessage());
        }
        Debug.log("Organization Party Id in Main DB is: " + mainDbOrgPartyId, module);
        if(UtilValidate.isEmpty(mainDbOrgPartyId)) {
            Debug.logError("Unable to create Organization party in Main DB", module);
            NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED",
                    "Creating Org Party (Main DB)", "Empty party Id found for org party in main db failed");
            return ServiceUtil.returnFailure(" Unable to create Organization party in Main DB for tenantId " + tenantId);
        }
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                "Done Creating Org Party (Main DB)", "Org party creation in Main DB completed successfully");

        // 3. Create Org Entry in TenantDB,
        Debug.logInfo("Creating Org Entry in Tenant DB, for tenant " + tenantId, module);
        String tenantDbOrgPartyId = null;
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                "Creating Organization (Tenant DB)", "Beginning to create organization in Tenant db");
        try {
            tenantDbOrgPartyId = createOrgPartyInTenantDB(tenantId, organizationName);
        } catch (OrgPartyCreationException e) {
            e.printStackTrace();
            NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED",
                    "Creating Organization (Main DB)", "Creating organization failed. Details: " + ExceptionUtils.getFullStackTrace(e));
            return ServiceUtil.returnFailure(e.getMessage());
        }
        Debug.logInfo("Organization Party Id in Tenant DB is: " + tenantDbOrgPartyId, module);

        // 4. create user login for given contact in Tenant DB, and build relationship
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                "Creating Admin UserLogin (Tenant DB)", "Beginning to create Admin user login in Tenant db");
        String adminUserLoginPartyId = createAdminUserLoginInTenantDb(tenantId, tenantDbOrgPartyId, contactFirstName, contactLastName, contactEmail, contactPassword );
        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                "Creating Admin UserLogin (Tenant DB)", "Creation of Admin user in tenant DB completed successfully");

        // 5. Send EMail notification
        if(UtilValidate.isNotEmpty(sendNotificationToContact) && "Y".equalsIgnoreCase(sendNotificationToContact)) {
            NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                    "Sending Email Notification", "Beginning to send email notification");
            Delegator tenantDelegator = DelegatorFactory.getDelegator("default#" + tenantId);
            Map<String,Object> emailNotificationCtx = UtilMisc.toMap(
                    "userLogin", UserLoginUtils.getSystemUserLogin(delegator),
                    "tenantId", tenantId,
                    "customerContactPartyId", adminUserLoginPartyId,
                    "customerContactEmail", contactEmail,
                    "customerOrganizationName", PartyHelper.getPartyName(delegator, mainDbOrgPartyId, false),
                    "customerContactPartyName", PartyHelper.getPartyName(tenantDelegator, adminUserLoginPartyId, false),
                    "customerContactInitialPassword", contactPassword
            );
            try {
                dispatcher.runAsync("sendNewCustomerOnboardedEmail", emailNotificationCtx);
            } catch (GenericServiceException e) {
                e.printStackTrace();
                NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "FAILED",
                        "Sending Email Notification", "Unable to send email notification. Details: " + ExceptionUtils.getFullStackTrace(e));
            }
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put("tenantId", tenantId);

        NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "COMPLETED", "Completed", "Customer On-boarding process completed successfully");
        return result;
    }

    /** Service to create Tenant and TenantDataSource entries
     *
     * @param ctx
     * @param context
     * @return
     */
    public static Map<String, Object> createTenantEntries(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Delegator delegator = ctx.getDelegator();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String tenantId = (String) context.get("tenantId");
        String organizationName = (String) context.get("organizationName");
        String dbHostIp = (String) context.get("dbHostIp");
        String dbHostPort = (String) context.get("dbHostPort");
        String tenantDbPrefix = (String) context.get("tenantDbPrefix");
        String tenantDbUsername = (String) context.get("tenantDbUsername");
        String tenantDbPassword = (String) context.get("tenantDbPassword");

        try {
            GenericValue newTenant = delegator.makeValue("Tenant");
            newTenant.setString("tenantId", tenantId);
            newTenant.setString("tenantName", organizationName);
            delegator.create(newTenant);

            //TODO: handle TenantDomainName in future

            GenericValue newTenantDataSourceOfbiz = delegator.makeValue("TenantDataSource");
            newTenantDataSourceOfbiz.setString("tenantId", tenantId);
            newTenantDataSourceOfbiz.setString("entityGroupName", "org.apache.ofbiz");
            newTenantDataSourceOfbiz.setString("jdbcUri", "jdbc:mysql://" + dbHostIp + ":" + dbHostPort + "/ofbiz_" + tenantDbPrefix);
            newTenantDataSourceOfbiz.setString("jdbcUsername", tenantDbUsername);
            newTenantDataSourceOfbiz.setString("jdbcPassword", tenantDbPassword);
            delegator.create(newTenantDataSourceOfbiz);

            GenericValue newTenantDataSourceOfbizOlap = delegator.makeValue("TenantDataSource");
            newTenantDataSourceOfbizOlap.setString("tenantId", tenantId);
            newTenantDataSourceOfbizOlap.setString("entityGroupName", "org.apache.ofbiz.olap");
            newTenantDataSourceOfbizOlap.setString("jdbcUri", "jdbc:mysql://" + dbHostIp + ":" + dbHostPort + "/ofbizolap_" + tenantDbPrefix);
            newTenantDataSourceOfbizOlap.setString("jdbcUsername", tenantDbUsername);
            newTenantDataSourceOfbizOlap.setString("jdbcPassword", tenantDbPassword);
            delegator.create(newTenantDataSourceOfbizOlap);

        } catch (GenericEntityException e) {
            e.printStackTrace();
            return ServiceUtil.returnFailure(e.getMessage());
        }
        return result;
    }

    private static String createAdminUserLoginInTenantDb(String tenantId, String orgPartyId, String contactFirstName, String contactLastName, String contactEmail, String contactPassword) {
        GenericDelegator tenantDelegator = null;
        LocalDispatcher tenantDispatcher = null;
        String adminUserLoginPartyId = null;

        if (UtilValidate.isNotEmpty(tenantId)) {
            tenantDelegator = (GenericDelegator) DelegatorFactory.getDelegator("default#" + tenantId);
            if (UtilValidate.isEmpty(tenantDelegator)) {
                Debug.logError("Invalid tenantId " + tenantId, module);
                return null;
            }
            tenantDispatcher = new GenericDispatcherFactory().createLocalDispatcher("default#" + tenantId, tenantDelegator);
        }
        if(UtilValidate.isEmpty(tenantDispatcher)) {
            // Unable to load tenant dispatcher
            Debug.logError("Unable to load tenant dispatcher/delegator for tenant " + tenantId, module);
            return null;
        }

        try {
            // Create Party & Person
            Map<String, Object> createPersonResp = tenantDispatcher.runSync("createPerson", UtilMisc.<String, Object> toMap("firstName", contactFirstName,
                    "lastName", contactLastName,
                    "userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator)));
            if(!ServiceUtil.isSuccess(createPersonResp)) {
                Debug.logError("Error creating new user for " + contactEmail + " in tenant " + tenantId, module);
                return null;
            }
            adminUserLoginPartyId = (String) createPersonResp.get("partyId");

            // Create UserLogin
            Map<String,Object> userLoginCtx = UtilMisc.toMap("userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator));
            userLoginCtx.put("userLoginId", contactEmail);
            userLoginCtx.put("currentPassword", contactPassword);
            userLoginCtx.put("currentPasswordVerify", contactPassword);
            userLoginCtx.put("requirePasswordChange", "Y"); // enforce password change for new user
            userLoginCtx.put("partyId", adminUserLoginPartyId);

            Map<String, Object> createUserLoginResp = tenantDispatcher.runSync("createUserLogin", userLoginCtx);
            if(!ServiceUtil.isSuccess(createUserLoginResp)) {
                Debug.logError("Error creating new UserLogin for " + contactEmail + " in tenant " + tenantId, module);
                return adminUserLoginPartyId;
            }

            // All Org Users should have EMPLOYEE role (so that we can fetch all org users)
            Map<String, Object> partyRole = UtilMisc.toMap(
                    "partyId", adminUserLoginPartyId,
                    "roleTypeId", "EMPLOYEE",
                    "userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator)
            );
            Map<String,Object> createPartyRoleResp = tenantDispatcher.runSync("createPartyRole", partyRole);
            if(!ServiceUtil.isSuccess(createPartyRoleResp)) {
                Debug.logError("Error creating new Party Role for " + contactEmail + " in tenant " + tenantId, module);
                return adminUserLoginPartyId;
            }

            // Add ONBOARDED_ADMIN Role to this admin user, so that we restrict any role changes for this user.
            Map<String, Object> onboardedAdminPartyRole = UtilMisc.toMap(
                    "partyId", adminUserLoginPartyId,
                    "roleTypeId", "ONBOARDED_ADMIN",
                    "userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator)
            );
            Map<String,Object> createOnboardedAdminPartyRoleResp = tenantDispatcher.runSync("createPartyRole", onboardedAdminPartyRole);
            if(!ServiceUtil.isSuccess(createOnboardedAdminPartyRoleResp)) {
                Debug.logError("Error creating new ONBOARDED_ADMIN Party Role for " + contactEmail + " in tenant " + tenantId, module);
                return adminUserLoginPartyId;
            }

            // Add partyRelationship with ORG Party (orgPartyId)
            Map<String, Object> partyRelationship = UtilMisc.toMap(
                    "partyIdFrom", orgPartyId,
                    "partyIdTo", adminUserLoginPartyId,
                    "roleTypeIdFrom", "ORGANIZATION_ROLE",
                    "roleTypeIdTo", "EMPLOYEE",
                    "partyRelationshipTypeId", "EMPLOYMENT",
                    "userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator)
            );
            Map<String,Object> createPartyRelationResp = tenantDispatcher.runSync("createPartyRelationship", partyRelationship);
            if(!ServiceUtil.isSuccess(createPartyRelationResp)) {
                Debug.logError("Error creating new Party Relationship between " + orgPartyId + " and " +adminUserLoginPartyId+" in tenant " + tenantId, module);
                return adminUserLoginPartyId;
            }

            // Assign SecurityGroup to user
            GenericValue userLoginSecurityGroup = tenantDelegator.makeValue("UserLoginSecurityGroup",
                    UtilMisc.toMap("userLoginId", contactEmail,
                            "groupId", SecurityGroupConstants.AP_FULLADMIN,
                            "fromDate", UtilDateTime.nowTimestamp()));
            try {
                userLoginSecurityGroup.create();
            } catch (GenericEntityException e) {
                Debug.logError("Error creating new Party Role for " + contactEmail + " in tenant " + tenantId, module);
                return adminUserLoginPartyId;
            }
        } catch (GenericServiceException e) {
            e.printStackTrace();
            return null;
        }
        return adminUserLoginPartyId;
    }

    /** Create an PartyGroup entry for the customer organization in Tenant DB
     *
     * @param tenantId
     * @param organizationName
     * @return
     */
    private static String createOrgPartyInTenantDB(String tenantId, String organizationName) throws OrgPartyCreationException {
        String tenantOrgPartyId = null;
        GenericDelegator tenantDelegator = null;
        LocalDispatcher tenantDispatcher = null;

        if (UtilValidate.isNotEmpty(tenantId)) {
            tenantDelegator = (GenericDelegator) DelegatorFactory.getDelegator("default#" + tenantId);
            if (UtilValidate.isEmpty(tenantDelegator)) {
                Debug.logError("Invalid tenantId " + tenantId, module);
                return null;
            }
            tenantDispatcher = new GenericDispatcherFactory().createLocalDispatcher("default#" + tenantId, tenantDelegator);
        }
        if(UtilValidate.isEmpty(tenantDispatcher)) {
            // Unable to load tenant dispatcher
            Debug.logError("Unable to load tenant dispatcher/delegator for tenant " + tenantId, module);
            throw new OrgPartyCreationException("unable to load dispatcher for tenant " + tenantId);
        }

        Map createPartyGroupCtx = UtilMisc.toMap(
                "userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator),
                "groupName", organizationName);
        Map<String, Object> createPartyGroupResponse = null;
        try {
            createPartyGroupResponse = tenantDispatcher.runSync("createPartyGroup", createPartyGroupCtx);
            Debug.logInfo("createPartyGroupResponse is: " + createPartyGroupResponse, module);
            tenantOrgPartyId = (String) createPartyGroupResponse.get("partyId");

        } catch (GenericServiceException e) {
            Debug.logError("Error creating an Org party in tenant DB for for tenant " + tenantId, module);
            e.printStackTrace();
        }

        Map<String, Object> createPartyGroupRoleCtx = UtilMisc.toMap("partyId", tenantOrgPartyId,
                "roleTypeId", "ORGANIZATION_ROLE",
                "userLogin", UserLoginUtils.getSystemUserLogin(tenantDelegator));
        try {
            Map createPartyGroupRoleResponse = tenantDispatcher.runSync("createPartyRole", createPartyGroupRoleCtx);
        } catch (GenericServiceException e) {
            Debug.logError("Error creating an Org party role in tenant DB for for tenant "
                    + tenantId + " and org party id: "  + tenantOrgPartyId, module);
            e.printStackTrace();
        }

        // Set Tenant Org Party Id in SystemProperty for easy lookup
        String organizationPartyKey = UtilProperties.getPropertyValue("admin.properties","customer.organization.party.key", "ORGANIZATION_PARTY_ID");
        GenericValue systemPropertyGv = tenantDelegator.makeValue("SystemProperty",
                UtilMisc.toMap("systemResourceId", "general",
                        "systemPropertyId", organizationPartyKey,
                        "systemPropertyValue", tenantOrgPartyId, "description", null));
        try {
            tenantDelegator.createOrStore(systemPropertyGv);
        } catch (GenericEntityException e) {
            Debug.logError("Error create a SystemProperty for tenants key: " + organizationPartyKey, module);
            e.printStackTrace();
        }
        //EntityUtilProperties.setPropertyValue(tenantDelegator, "general", organizationPartyKey, tenantOrgPartyId);

        return tenantOrgPartyId;
    }

    /**
     * Create an PartyGroup entry for the customer organization
     * And associate with tenantId - for easy lookup
     * @param dispatcher
     * @param userLogin
     * @param tenantId
     * @param organizationName
     * @return
     */
    private static String createOrgPartyInMainDB(LocalDispatcher dispatcher, GenericValue userLogin, String tenantId, String organizationName) throws OrgPartyCreationException {
        String orgPartyId = null;
        Delegator delegator = dispatcher.getDelegator();

        try {
            Map<String,Object> createPartyGroupCtx = UtilMisc.toMap(
                    "userLogin", userLogin,
                    "groupName", organizationName
                    );
            Map<String, Object> createPartyGroupResponse = dispatcher.runSync("createPartyGroup", createPartyGroupCtx);
            Debug.logInfo("createPartyGroupResponse is: " + createPartyGroupResponse, module);
            orgPartyId = (String) createPartyGroupResponse.get("partyId");

            if(UtilValidate.isEmpty(orgPartyId)) {
                // Unable to create;
                throw new OrgPartyCreationException("Unable to create org party in main db, for tenant " + tenantId);
            }

            // Create AP_CUSTOMER Role to this org party
            Map<String, Object> createPartyGroupRoleCtx = UtilMisc.toMap("partyId", orgPartyId,
                    "roleTypeId", "AP_CUSTOMER",
                    "userLogin", userLogin);
            Map createPartyGroupRoleResponse = dispatcher.runSync("createPartyRole", createPartyGroupRoleCtx);

            //Add Entry in Main DB: TenantOrgParty (tenantId - orgPartyId) for easy lookup
            GenericValue tenantOrgParty = delegator.makeValue("TenantOrgParty");
            tenantOrgParty.setString("orgPartyId", orgPartyId);
            tenantOrgParty.setString("tenantId", tenantId);

            delegator.create(tenantOrgParty);
        } catch (GenericEntityException | GenericServiceException e) {
            e.printStackTrace();
            throw new OrgPartyCreationException("error trying to create a org party in main db, for tenant " + tenantId);
        }
        return orgPartyId;
    }


    private static boolean createTenantDatabase(LocalDispatcher dispatcher, String newTenantTransactionId, String dbHostname, String dbHostPort, String dbUsername, String dbPassword,
                                      String tenantId, String tenantDbUsername, String tenantDbPassword) {
        String cloneScriptFile = getCloneScriptFilePath();
        Debug.logInfo("Cloning Database from template for new tenant: " + tenantId, module);
        Debug.logInfo("Running Script: " + cloneScriptFile, module);
        try {
            Debug.logInfo(cloneScriptFile + " " + dbHostname + " "
                    + dbUsername + " ******* " + dbHostPort + " "
                    + tenantId + " " + tenantDbUsername + " ******" , module);

            ProcessBuilder pb = null;
            if (SystemUtils.IS_OS_WINDOWS) {
                pb = new ProcessBuilder(cloneScriptFile, dbHostname, dbUsername, dbPassword, dbHostPort,
                        tenantId, tenantDbUsername, tenantDbPassword);
            } else {
                pb = new ProcessBuilder("sh", cloneScriptFile, dbHostname, dbUsername, dbPassword, dbHostPort,
                        tenantId, tenantDbUsername, tenantDbPassword);
            }
            Process process = pb.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String log;
            StringBuilder scriptLogs = new StringBuilder();
            while ((log = reader.readLine()) != null) {
                Debug.logInfo(">>> " + log, module);
                scriptLogs.append(log);
                scriptLogs.append("<br/>");
            }
            int scriptExitCode = process.waitFor();
            Debug.logInfo("Script Logs:\n" + scriptLogs.toString(), module);

            if(scriptExitCode == 0) {
                // Success
                Debug.logInfo("Tenant Database creation completed for tenant: " + tenantId, module);
                NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                        "Completed Creating Databases", "Tenant Database creation completed with exit code 0. \nLogs: " + scriptLogs.toString());
            } else {
                // Error code
                Debug.logInfo("Script execution returned error code. Exit Code:" + scriptExitCode, module);
                NewTenantTransactionLogUtils.logTransactionStep(dispatcher, newTenantTransactionId, "IN-PROGRESS",
                        "Failed Creating Databases", "Tenant Database creation completed with exit code " + scriptExitCode  +"\n Logs: " + scriptLogs.toString());
            }
            return scriptExitCode == 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // Return the script filename based on OS
    private static String getCloneScriptFilePath() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return "tools/scripts/windows/clone-tenant-db.cmd";
        } else {
            return "tools/scripts/linux/clone-tenant-db.sh";
        }
    }


    /**
     * This is a test service to test/verify Cmd/Shell script...
     *
     * @param ctx
     * @param context
     * @return
     */
    public static Map<String, Object> testShellScriptService(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Delegator delegator = ctx.getDelegator();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        Debug.logInfo("Service to test/verify cmd/shell script...", module);

        ProcessBuilder pb = null;
        if (SystemUtils.IS_OS_WINDOWS) {
            pb = new ProcessBuilder("tools/scripts/windows/test.cmd", "someinput");
        } else {
            pb = new ProcessBuilder("sh", "tools/scripts/linux/test.sh", "someinput");
        }
        Process process = null;
        try {
            process = pb.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String log;
            StringBuilder scriptLogs = new StringBuilder();
            while ((log = reader.readLine()) != null) {
                Debug.logInfo(">>> " + log, module);
                scriptLogs.append(log);
                scriptLogs.append("\n");
            }
            int scriptExitCode = process.waitFor();
            Debug.logInfo("Script Exit Code: " + scriptExitCode, module);
            Debug.logInfo("Script Logs:\n" + scriptLogs.toString(), module);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }


}
